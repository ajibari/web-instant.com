---
title: 'Tes judul dari blogging'
taxonomy:
    category:
        - blog
    tag:
        - tag1
        - tag2
visible: true
content:
    items: '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

tes blogging yak