---
title: Blog
published: true
process:
    markdown: true
    twig: true
twig_first: true
routable: true
visible: true
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: '1'
---

<div id="wrapper" class="win-min-height">
    <section id="block15c" class="block background01 animatedParent animateOnce" zp-module="Blog">
      <div class="holder">
        <div class="boxes animated fadeIn slow">
          
          {% for post in page.find('/blog').children.order('date', 'desc').slice(0, 5) %}
            
            {% set bannerimage = post.media['header.jpg'] %}
			
            <div class="box">
              {% if bannerimage %}
                <img class="img-responsive" src="{{ post.media['header.jpg'].url }}" alt="{{ post.title }}">
              {% else %}
                <img class="img-responsive" src="{{ url('theme://images/black.png') }}" alt="{{ post.title }}">
              {% endif %}
              <div class="caption">
                <div class="inner-caption color01 col-md-4 col-sm-4">
                  <h3 style="padding-left: 2em">{{ post.title }}</h3>
                  <!--<p></p>-->
                </div>
              <span class="readmore"><a class="btn more color01 color01-hover" href="#"><i class="fa fa-arrow-right">&nbsp;</i></a></span>
              </div>
            </div>

          {% endfor %}

        </div>
      </div>
  </section>
</div>