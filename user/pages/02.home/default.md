---
title: Home
published: true
jscomments:
    active: true
    provider: disqus
process:
    markdown: true
    twig: true
twig_first: true
routable: true
---

<section id="block02c" class="block background01" data-module="Slider">
        <div class="row"> 
          <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
            <div class="slides" data-group="slides">
              <ul>
                <li>
                  <div class="slide-body" data-group="slide"> <img src="{{ url('theme://images/slide04.jpg') }}" alt="Slide04">
                   <div class="slide_textholder">
                   <div class="holderinner"> <div class="caption_outer caption" data-animate="slideAppearLeftToRight">
                   <div class="caption_inner"><div class="caption subheader">
                      <h1 class="color01">We offer services <br> for develop <br> your web app</h1>
                      
                    </div>
                    <div class="caption learnmore"><a class="btn more color01 color01-hover goto" href="#" data-goto="block12e">Contact Us</a></div>
                     <div class="layer">&nbsp;</div>
                    </div>
                    
                    </div>
                    </div></div>
                  </div>
                </li>
               <li>
                  <div class="slide-body" data-group="slide"> <img src="{{ url('theme://images/meeting-work.jpeg') }}" alt="Slide04">
                   <div class="slide_textholder">
                   <div class="holderinner"> <div class="caption_outer caption" data-animate="slideAppearLeftToRight">
                   <div class="caption_inner"><div class="caption subheader">
                      <h1 class="color01">Custom web app<br>Online store<br>to landing page</h1>
                      
                    </div>
                    <div class="caption learnmore"> <a class="btn more color01 color01-hover goto" href="#" data-goto="block12e">Contact Us</a></div>
                     <div class="layer">&nbsp;</div>
                    </div>
                    
                    </div>
                    </div></div>
                  </div>
                </li>
                
              </ul>
            </div>
           <a class="slider-control left color01-hover" href="#" data-jump="prev"><i class="fa fa-chevron-left">&nbsp;</i></a>
            <a class="slider-control right color01-hover" href="#" data-jump="next"><i class="fa fa-chevron-right">&nbsp;</i></a>
          </div>
        </div>
</section>
<section id="block12e" class="block animatedParent animateOnce" data-module="Contact">
    <div class="container">
      	<div class="row animated fadeIn slow">
        <div class="quickmail">
       		<div class="form col-md-4 col-sm-6 col-xs-12">
            <h2 class="color08">Contact Us</h2>
                <p class=" color02">If you have a project or want to discuss something about it. Don't hesitate to contact us :)</p>
                	<form class="form-inline" action="form_process.php" method="POST">
                    <div class="form-group">
                      <input type="text" class="form-control color02 background01 border-color09" id="exampleInputName2" placeholder="Name">
                    </div>
                    <div class="form-group">
                      <input type="email" class="form-control color02 background01 border-color09" id="exampleInputEmail2" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control color02 background01 border-color09" id="exampleInputName2" placeholder="how we can help?">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control color02 background01 border-color09" id="exampleInputName2" placeholder="budget">
                    </div>
                    <div class="form-group fullwidth">
                    <textarea class="form-control color02 background01 border-color09" rows="3" placeholder="type your message here"></textarea>
                    </div>
                    <div class="submitbutton fullwidth">
                    	<button type="submit" class="btn more background07 color01 color01-hover04">request a quote</button>
                    </div>
                  </form>
        </div>
                <div class="map_holder col-md-8 col-sm-6 col-xs-12"><iframe src="https://www.google.com/maps?q=-6.340385,106.858828&hl=es;z%3D14&amp;output=embed" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe></div>
        </div>
      
      </div>
      </div>
</section>
<section id="block10a" class="block background01 animatedParent animateOnce" zp-module="Team">
    <div class="container">
      	<div class="row">
        <div class="team">
        	<h2 class="animated fadeIn slow">Teams</h2>
        	<div class="boxes animated fadeIn slow">
                <div class="col col-sm-1"></div>
                <div class="col col-sm-2 col-xs-6">
                <div class="icone_box"><img src="{{ page.media['bari.jpg'].url }}" alt="Creatives">
                	<div class="caption background08a">
                    <div class="text-holder color01">
                      <h3>Ridwan Aji Bari</h3>
    				          <p>Fullstack Developer</p>
                      </div>
                    	<div class="social_share">
                                <a class="btn more color01 color01-hover" href="http://www.twitter.com/ajibari"><i class="fa fa-twitter">&nbsp;</i></a>
                                <a class="btn more color01 color01-hover" href="https://www.linkedin.com/in/ridwan-aji-bari-26a85937/"><i class="fa fa-linkedin">&nbsp;</i></a>
                      </div>
                    </div>
                </div>
                </div>
                <div class="col col-sm-2 col-xs-6">
                 <div class="icone_box"><img src="{{ url('theme://images/jajang-pic.png') }}" alt="Creatives">
                 	<div class="caption background08a">
                    <div class="text-holder color01">
                <h3>Jajang Jaenudin</h3>
				<p>Fullstack Developer</p>
                </div>
                 	<div class="social_share">
                    	 <a class="btn more color01 color01-hover" href="#"><i class="fa fa-facebook">&nbsp;</i></a>
                            <a class="btn more color01 color01-hover" href="#"><i class="fa fa-twitter">&nbsp;</i></a>
                            <a class="btn more color01 color01-hover" href="#"><i class="fa fa-google-plus">&nbsp;</i></a>
                    </div>
                    </div>
                 </div>
                </div>
                <div class="col col-sm-2 col-xs-6">
                 <div class="icone_box"><img src="{{ url('theme://images/dwiki-pic.png') }}" alt="Creatives">
                	<div class="caption background08a">
                    <div class="text-holder color01">
                    <h3>Dwiki</h3>
                    <p>Fullstack Developer</p>
                </div>
                 	<div class="social_share">
                    	 <a class="btn more color01 color01-hover" href="#"><i class="fa fa-facebook">&nbsp;</i></a>
                            <a class="btn more color01 color01-hover" href="#"><i class="fa fa-twitter">&nbsp;</i></a>
                            <a class="btn more color01 color01-hover" href="#"><i class="fa fa-google-plus">&nbsp;</i></a>
                    </div>
                    </div>
                 </div>
                </div>
                <div class="col col-sm-2 col-xs-6">
                 <div class="icone_box"><img src="{{ url('theme://images/roni-pic.png') }}" alt="Creatives">
                 	<div class="caption background08a">
                    <div class="text-holder color01">
                    <h3>Roni</h3>
                    <p>Backend Engineer</p>
                </div>
                 	<div class="social_share">
                    	 <a class="btn more icone01 color01 color01-hover" href="#"><i class="fa fa-facebook">&nbsp;</i></a>
                            <a class="btn more icone02 color01 color01-hover" href="#"><i class="fa fa-twitter">&nbsp;</i></a>
                            <a class="btn more icone03 color01 color01-hover" href="#"><i class="fa fa-google-plus">&nbsp;</i></a>
                    </div>
                    </div>
                 </div>
                </div>
                <div class="col col-sm-2 col-xs-6">
                 <div class="icone_box"><img src="{{ page.media['dhanis.jpg'].url }}" alt="Web Designer">
                  <div class="caption background08a">
                    <div class="text-holder color01">
                    <h3>Dhanis</h3>
                    <p>Web Designer</p>
                </div>
                  <div class="social_share">
                       <a class="btn more icone01 color01 color01-hover" href="http://www.facebook.com/ramadhanis.putra"><i class="fa fa-facebook">&nbsp;</i></a>
                            <a class="btn more icone02 color01 color01-hover" href="http://twitter.com/dhanisputra"><i class="fa fa-twitter">&nbsp;</i></a>
                            <a class="btn more icone03 color01 color01-hover" href="http://linkedin.com/in/dhanisputra"><i class="fa fa-linkedin">&nbsp;</i></a>
                    </div>
                    </div>
                 </div>
                </div>
                <div class="col col-sm-1"></div>
            </div>
            </div>
        </div>
      </div>
</section>
<!--<section id="block06e" class="block background01 animatedParent animateOnce" data-module="Work">
  	<div class="container">
    	<div class="row">
        	<div class="projects">
            	<h2 class="animated fadeIn slow">what we have done</h2>
                <div id="Carousel06d" class="carousel slide">
                
                <div class="portfolio">
                <div id="ourprojects" class="carousel-inner">
                <div class="holder animated fadeIn slow">
                    	<div class="img-box col-md-4 col-sm-4 col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/project12.jpg" alt="Project"></a>
                        </div>
                        <div class="img-box col-md-4 col-sm-4 col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/project13.jpg" alt="Project"></a>
                        </div>
                        <div class="img-box col-md-4 col-sm-4 col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/project14.jpg" alt="Project"></a>
                        </div>
                        <div class="img-box col-md-4 col-sm-4 col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/project15.jpg" alt="Project"></a>
                        </div>
                        <div class="img-box col-md-4 col-sm-4 col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/project16.jpg" alt="Project"></a>
                        </div>
                        <div class="img-box col-md-4 col-sm-4 col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/project17.jpg" alt="Project"></a>
                        </div>
                        <div class="img-box col-md-4 col-sm-4 col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/project18.jpg" alt="Project"></a>
                        </div>
                        <div class="img-box col-md-4 col-sm-4 col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/project19.jpg" alt="Project"></a>
                        </div>
                        <div class="img-box col-md-4 col-sm-4 col-xs-6">
                        <a href="#"><img class="img-responsive" src="images/project20.jpg" alt="Project"></a>
                        </div>
                    </div>  
                </div>
                <div class="holder detail animated fadeIn slow">
                 	<span>Seems Interesting?</span>
                    <a class="btn more background07 color01 color01-hover01 border-color02 goto" href="#" data-goto="block12e">Contact Us</a>
                 </div>
                </div>
                
                </div>
            </div>
         </div>
    </div>
</section>-->
        