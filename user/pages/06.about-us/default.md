---
title: 'About Us'
published: true
process:
    markdown: true
    twig: true
twig_first: true
routable: true
cache_enable: true
---

<div class="container">
      	<div class="row">
        <div class="feature animated fadeIn slow">
        	<h2>we love what we do</h2>
            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
            <a href="javascript:void(0)" class="btn btn-default more color01 background08 color01-hover border-color07">LEARN MORE</a>
        </div>
        	<div class="boxes animated fadeIn slow">
              <div class="img-area col-md-10 col-md-offset-1">
              <img class="img-responsive" src="{{post.url}}images/work09.jpg" alt="Work">
              </div> 
            </div>
</div>