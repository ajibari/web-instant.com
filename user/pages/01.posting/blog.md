---
title: Posting
published: true
taxonomy:
    category:
        - blog
        - animal
    tag:
        - post
        - selfie
jscomments:
    active: true
    provider: disqus
content:
    items: '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

tes halaman posting