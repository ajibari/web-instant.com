<?php

/* partials/footer.html.twig */
class __TwigTemplate_d965c6d1f5de92a8eb843afd51dcc61b9ab606bbb1420c2162c71cbbbba5e0c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer id=\"block13c\" class=\"block background02 animatedParent animateOnce\" data-module=\"Footer\">
    <div class=\"container\">
      \t<div class=\"row\">
        \t<div class=\"footerinner animated fadeIn slow\">
            <div class=\"block13b1\">
            <div class=\"holder\">
              <div class=\"col-md-9 col-sm-9 col-xs-7 col_left\">
               <nav class=\"navbar navbar-default\">
          \t\t\t\t<ul class=\"nav navbar-nav color04\">
                  <li class=\"active\"><a href=\"#\">Home <span class=\"sr-only\">(current)</span></a></li>
                  <li><a class=\"color01 color01-hover02\" href=\"";
        // line 11
        echo ($context["base_url"] ?? null);
        echo "/about-us\">About Us</a></li>
                  <li><a class=\"color01 color01-hover02\" href=\"";
        // line 12
        echo ($context["base_url"] ?? null);
        echo "/blog\">Blog</a></li>
                  <!--<li><a class=\"color01 color01-hover02\" href=\"#\">Project</a></li>-->
                  ";
        // line 14
        if ((($context["base_url"] ?? null) == $this->getAttribute(($context["page"] ?? null), "url", array()))) {
            // line 15
            echo "                  <li><a id =\"tes\" class=\"color01 color01-hover02 goto\" href=\"#\" data-goto=\"block12e\">Contact</a></li>
                  ";
        } else {
            // line 17
            echo "                  <li><a id =\"tes\" class=\"color01 color01-hover02 goto\" href=\"";
            echo ($context["base_url"] ?? null);
            echo "/#block12e\" data-goto=\"block12e\">Contact</a></li>
                  ";
        }
        // line 19
        echo "                  <li><a class=\"color01 color01-hover02\" href=\"";
        echo ($context["base_url"] ?? null);
        echo "/services\">Services</a></li>
                  </ul>
                </nav>
              </div>
            <div class=\"col-md-3 col-sm-3 col-xs-5 col_right\">
\t\t\t<div class=\"social-icon\">
        <ul>
        \t
            <li><a class=\"color01 color01-hover01\" href=\"https://twitter.com/WebInstantcom1\"><i class=\"fa fa-twitter\">&nbsp;</i></a></li>
            <li><a class=\"color01 color01-hover01\" href=\"https://www.facebook.com/webinstantcom/\"><i class=\"fa fa-facebook\">&nbsp;</i></a></li>
            
        </ul>
\t\t
        </div>
            </div>
            </div>
            <div class=\"footer_logo col-md-12\">
            <a class=\"navbar-brand\" href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 36
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/web-instant.png");
        echo "\" alt=\"Web-instant.com\"></a>
            <span class=\"color01\">© web-instant.com</span>
            </div>
            </div>
            </div>
        </div>
      
      </div>
  </footer>";
    }

    public function getTemplateName()
    {
        return "partials/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 36,  52 => 19,  46 => 17,  42 => 15,  40 => 14,  35 => 12,  31 => 11,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer id=\"block13c\" class=\"block background02 animatedParent animateOnce\" data-module=\"Footer\">
    <div class=\"container\">
      \t<div class=\"row\">
        \t<div class=\"footerinner animated fadeIn slow\">
            <div class=\"block13b1\">
            <div class=\"holder\">
              <div class=\"col-md-9 col-sm-9 col-xs-7 col_left\">
               <nav class=\"navbar navbar-default\">
          \t\t\t\t<ul class=\"nav navbar-nav color04\">
                  <li class=\"active\"><a href=\"#\">Home <span class=\"sr-only\">(current)</span></a></li>
                  <li><a class=\"color01 color01-hover02\" href=\"{{ base_url }}/about-us\">About Us</a></li>
                  <li><a class=\"color01 color01-hover02\" href=\"{{ base_url }}/blog\">Blog</a></li>
                  <!--<li><a class=\"color01 color01-hover02\" href=\"#\">Project</a></li>-->
                  {% if base_url == page.url %}
                  <li><a id =\"tes\" class=\"color01 color01-hover02 goto\" href=\"#\" data-goto=\"block12e\">Contact</a></li>
                  {% else %}
                  <li><a id =\"tes\" class=\"color01 color01-hover02 goto\" href=\"{{base_url}}/#block12e\" data-goto=\"block12e\">Contact</a></li>
                  {% endif %}
                  <li><a class=\"color01 color01-hover02\" href=\"{{ base_url }}/services\">Services</a></li>
                  </ul>
                </nav>
              </div>
            <div class=\"col-md-3 col-sm-3 col-xs-5 col_right\">
\t\t\t<div class=\"social-icon\">
        <ul>
        \t
            <li><a class=\"color01 color01-hover01\" href=\"https://twitter.com/WebInstantcom1\"><i class=\"fa fa-twitter\">&nbsp;</i></a></li>
            <li><a class=\"color01 color01-hover01\" href=\"https://www.facebook.com/webinstantcom/\"><i class=\"fa fa-facebook\">&nbsp;</i></a></li>
            
        </ul>
\t\t
        </div>
            </div>
            </div>
            <div class=\"footer_logo col-md-12\">
            <a class=\"navbar-brand\" href=\"#\"><img class=\"img-responsive\" src=\"{{ url('theme://images/web-instant.png') }}\" alt=\"Web-instant.com\"></a>
            <span class=\"color01\">© web-instant.com</span>
            </div>
            </div>
            </div>
        </div>
      
      </div>
  </footer>", "partials/footer.html.twig", "C:\\xampp7\\htdocs\\web-instant.com\\user\\themes\\webinstant\\templates\\partials\\footer.html.twig");
    }
}
