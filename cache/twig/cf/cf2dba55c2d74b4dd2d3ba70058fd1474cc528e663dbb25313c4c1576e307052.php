<?php

/* @Page:C:/xampp7/htdocs/web-instant.com/user/pages/05.blog */
class __TwigTemplate_7dff1b7604404df289966d926007f1fd516803c6baaad93370f5c47ebee8bc73 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"wrapper\" class=\"win-min-height\">
    <section id=\"block15c\" class=\"block background01 animatedParent animateOnce\" zp-module=\"Blog\">
      <div class=\"holder\">
        <div class=\"boxes animated fadeIn slow\">
          
          ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "find", array(0 => "/blog"), "method"), "children", array()), "order", array(0 => "date", 1 => "desc"), "method"), "slice", array(0 => 0, 1 => 5), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 7
            echo "            
            ";
            // line 8
            $context["bannerimage"] = $this->getAttribute($this->getAttribute($context["post"], "media", array()), "header.jpg", array(), "array");
            // line 9
            echo "\t\t\t
            <div class=\"box\">
              ";
            // line 11
            if (($context["bannerimage"] ?? null)) {
                // line 12
                echo "                <!--<img class=\"img-responsive\" src=\"";
                echo ($context["bannerimage"] ?? null);
                echo "\" alt=\"Blog\">-->
                ";
                // line 13
                echo ($context["bannerimage"] ?? null);
                echo "
              ";
            } else {
                // line 15
                echo "                <img class=\"img-responsive\" src=\"";
                echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/black.png");
                echo "\" alt=\"Blog\">
              ";
            }
            // line 17
            echo "              <div class=\"caption\">
                <div class=\"inner-caption color01 col-md-4 col-sm-4\">
                  <h3 style=\"padding-left: 2em\">";
            // line 19
            echo $this->getAttribute($context["post"], "title", array());
            echo "</h3>
                  <!--<p></p>-->
                </div>
              <span class=\"readmore\"><a class=\"btn more color01 color01-hover\" href=\"#\"><i class=\"fa fa-arrow-right\">&nbsp;</i></a></span>
              </div>
            </div>

          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "
        </div>
      </div>
  </section>
</div>";
    }

    public function getTemplateName()
    {
        return "@Page:C:/xampp7/htdocs/web-instant.com/user/pages/05.blog";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 27,  61 => 19,  57 => 17,  51 => 15,  46 => 13,  41 => 12,  39 => 11,  35 => 9,  33 => 8,  30 => 7,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"wrapper\" class=\"win-min-height\">
    <section id=\"block15c\" class=\"block background01 animatedParent animateOnce\" zp-module=\"Blog\">
      <div class=\"holder\">
        <div class=\"boxes animated fadeIn slow\">
          
          {% for post in page.find('/blog').children.order('date', 'desc').slice(0, 5) %}
            
            {% set bannerimage = post.media['header.jpg'] %}
\t\t\t
            <div class=\"box\">
              {% if bannerimage %}
                <!--<img class=\"img-responsive\" src=\"{{ bannerimage }}\" alt=\"Blog\">-->
                {{ bannerimage }}
              {% else %}
                <img class=\"img-responsive\" src=\"{{ url('theme://images/black.png') }}\" alt=\"Blog\">
              {% endif %}
              <div class=\"caption\">
                <div class=\"inner-caption color01 col-md-4 col-sm-4\">
                  <h3 style=\"padding-left: 2em\">{{ post.title }}</h3>
                  <!--<p></p>-->
                </div>
              <span class=\"readmore\"><a class=\"btn more color01 color01-hover\" href=\"#\"><i class=\"fa fa-arrow-right\">&nbsp;</i></a></span>
              </div>
            </div>

          {% endfor %}

        </div>
      </div>
  </section>
</div>", "@Page:C:/xampp7/htdocs/web-instant.com/user/pages/05.blog", "");
    }
}
