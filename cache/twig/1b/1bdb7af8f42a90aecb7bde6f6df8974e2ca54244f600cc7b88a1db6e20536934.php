<?php

/* partials/base.html.twig */
class __TwigTemplate_2612bda0fcb469424b3f6e215e695c1a89194281ad2a5dcde5f096b734d2e464 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'header' => array($this, 'block_header'),
            'showcase' => array($this, 'block_showcase'),
            'feature' => array($this, 'block_feature'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'bottom' => array($this, 'block_bottom'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["theme_config"] = $this->getAttribute($this->getAttribute(($context["config"] ?? null), "themes", array()), $this->getAttribute($this->getAttribute($this->getAttribute(($context["config"] ?? null), "system", array()), "pages", array()), "theme", array()));
        // line 2
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 3
        echo (($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getLanguage", array())) ? ($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getLanguage", array())) : ("en"));
        echo "\">
<head>
";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 34
        echo "</head>

<body class=\"home\">
    <div id=\"wrapper\" class=\"win-min-height\">
        ";
        // line 38
        $this->displayBlock('header', $context, $blocks);
        // line 104
        echo "        


        ";
        // line 107
        $this->displayBlock('showcase', $context, $blocks);
        // line 108
        echo "        ";
        $this->displayBlock('feature', $context, $blocks);
        // line 109
        echo "
        ";
        // line 110
        $this->displayBlock('body', $context, $blocks);
        // line 116
        echo "
        ";
        // line 117
        $this->displayBlock('footer', $context, $blocks);
        // line 120
        echo "
        ";
        // line 121
        $this->displayBlock('bottom', $context, $blocks);
        // line 124
        echo "
    </div>
    ";
        // line 126
        $this->displayBlock('javascripts', $context, $blocks);
        // line 139
        echo "    ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
</body>
</html>";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    <meta charset=\"utf-8\" />
    <title>";
        // line 7
        if ($this->getAttribute(($context["header"] ?? null), "title", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute(($context["header"] ?? null), "title", array()), "html");
            echo " | ";
        }
        echo twig_escape_filter($this->env, $this->getAttribute(($context["site"] ?? null), "title", array()), "html");
        echo "</title>
    ";
        // line 8
        $this->loadTemplate("partials/metadata.html.twig", "partials/base.html.twig", 8)->display($context);
        // line 9
        echo "    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 12
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/favicon.png");
        echo "\" />
    <link rel=\"canonical\" href=\"";
        // line 13
        echo $this->getAttribute(($context["page"] ?? null), "url", array(0 => true, 1 => true), "method");
        echo "\" />

    ";
        // line 15
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 29
        echo "    ";
        echo $this->getAttribute(($context["assets"] ?? null), "css", array(), "method");
        echo "

    

";
    }

    // line 15
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 16
        echo "        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        ";
        // line 17
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css/bootstrap.min.css", 1 => 103), "method");
        // line 18
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css/font-awesome.min.css", 1 => 102), "method");
        // line 19
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css/animation.css", 1 => 101), "method");
        // line 20
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css/normalize.css", 1 => 100), "method");
        // line 21
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css/style.css", 1 => 100), "method");
        // line 22
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css/royalslider.css"), "method");
        // line 23
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css/pace_theme.css"), "method");
        // line 24
        echo "
        ";
        // line 25
        if (((($this->getAttribute(($context["browser"] ?? null), "getBrowser", array()) == "msie") && ($this->getAttribute(($context["browser"] ?? null), "getVersion", array()) >= 8)) && ($this->getAttribute(($context["browser"] ?? null), "getVersion", array()) <= 9))) {
            // line 26
            echo "            ";
            $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/html5shiv-printshiv.min.js"), "method");
            // line 27
            echo "        ";
        }
        // line 28
        echo "    ";
    }

    // line 38
    public function block_header($context, array $blocks = array())
    {
        // line 39
        echo "        <header id=\"block01c\" class=\"header block background01\" data-module=\"Header\">
            <div class=\"container header_block\">
                <div class=\"row\">
                    <div class=\"social-icon\">
                        <ul>
                            <li>
                                <a class=\"colorWhite color01-hover03\" href=\"https://www.facebook.com/webinstantcom/\">
                                <i class=\"fa fa-facebook\">&nbsp;</i></a>
                            </li>
                            <li>
                                <a class=\"colorWhite color01-hover03\" href=\"https://twitter.com/WebInstantcom1\">
                                <i class=\"fa fa-twitter\">&nbsp;</i></a>
                            </li>
                        </ul>
                    </div>
                    <nav class=\"navbar navbar-default\">
                        <div class=\"container-fluid\">
                          <div class=\"navbar-header\">
                          <a class=\"navbar-brand\" href=\"";
        // line 57
        echo ($context["base_url"] ?? null);
        echo "\"><img class=\"img-responsive\" src=\"";
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/web-instant.png");
        echo "\" alt=\"Web-instant\"></a>
                          <div class=\"menu-btn\">
                            <button type=\"button\" class=\"navbar-toggle collapsed colorWhite\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                              <strong>
                              <span class=\"sr-only\">Toggle navigation</span>
                              <span class=\"icon-bar background02\"></span>
                              <span class=\"icon-bar background02\"></span>
                              <span class=\"icon-bar background02\"></span>
                              </strong>
                             <strong> MENU</strong>
                            </button></div>
                          </div>
                          
                          <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                            <ul class=\"nav navbar-nav colorWhite\">
                              <li class=\"active\"><a class=\"colorWhite color01-hover03\" href=\"";
        // line 72
        echo ($context["base_url"] ?? null);
        echo "\">Home <span class=\"sr-only\">(current)</span></a></li>
                              <li><a class=\"colorWhite color01-hover03\" href=\"";
        // line 73
        echo ($context["base_url"] ?? null);
        echo "/blog\">Blog</a></li>
                              <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle colorWhite color01-hover03\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">Services <span class=\"caret\"></span></a>
                                <ul class=\"dropdown-menu background01\" role=\"menu\">
                                  <li><a class=\"colorWhite color01-hover03\" href=\"";
        // line 77
        echo ($context["base_url"] ?? null);
        echo "/custom-web-application\">Custom web aplication</a></li>
                                  <li class=\"divider\"></li>
                                  <li><a class=\"colorWhite color01-hover03\" href=\"";
        // line 79
        echo ($context["base_url"] ?? null);
        echo "/wordpress\">Wordpress</a></li>
                                  <li class=\"divider\"></li>
                                  <li><a class=\"colorWhite color01-hover03\" href=\"";
        // line 81
        echo ($context["base_url"] ?? null);
        echo "/coming-soon-landing-page\">Coming soon page/ Landing page</a></li>
                                  <li class=\"divider\"></li>
                                  <li><a class=\"colorWhite color01-hover03\" href=\"";
        // line 83
        echo ($context["base_url"] ?? null);
        echo "/invitation-online\">Invitation Online</a></li>
                                  <li class=\"divider\"></li>
                                  <li><a class=\"colorWhite color01-hover03\" href=\"#\"></a></li>
                                </ul>
                              </li>
                              <li>
                                ";
        // line 89
        if ((($context["base_url"] ?? null) == $this->getAttribute(($context["page"] ?? null), "url", array()))) {
            // line 90
            echo "                                    <a class=\"colorWhite color01-hover03 goto\" href=\"#\" data-goto=\"block12e\">Contact Us</a>
                                ";
        } else {
            // line 92
            echo "                                    <a class=\"colorWhite color01-hover03 goto\" href=\"";
            echo ($context["base_url"] ?? null);
            echo "#block12e\">Contact Us</a>
                                ";
        }
        // line 94
        echo "                              </li>
                            </ul>
                            
                          </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        ";
    }

    // line 107
    public function block_showcase($context, array $blocks = array())
    {
    }

    // line 108
    public function block_feature($context, array $blocks = array())
    {
    }

    // line 110
    public function block_body($context, array $blocks = array())
    {
        // line 111
        echo "
        <section id=\"body\" class=\"";
        // line 112
        echo ($context["class"] ?? null);
        echo "\">
            ";
        // line 113
        $this->displayBlock('content', $context, $blocks);
        // line 114
        echo "        </section>
        ";
    }

    // line 113
    public function block_content($context, array $blocks = array())
    {
    }

    // line 117
    public function block_footer($context, array $blocks = array())
    {
        // line 118
        echo "            ";
        $this->loadTemplate("partials/footer.html.twig", "partials/base.html.twig", 118)->display($context);
        // line 119
        echo "        ";
    }

    // line 121
    public function block_bottom($context, array $blocks = array())
    {
        // line 122
        echo "
        ";
    }

    // line 126
    public function block_javascripts($context, array $blocks = array())
    {
        // line 127
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/jquery-1.11.2.min.js", 1 => 100), "method");
        // line 128
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/bootstrap.min.js", 1 => 100), "method");
        // line 129
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/pace.min.js", 1 => 100), "method");
        // line 130
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/responsive-slider.js"), "method");
        // line 131
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/countUp.js"), "method");
        // line 132
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/touch-slide.js"), "method");
        // line 133
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/modernizr.js"), "method");
        // line 134
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/classie.js"), "method");
        // line 135
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/jquery.custom-scrollbar.js"), "method");
        // line 136
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/royalslider-min.js"), "method");
        // line 137
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/custom.js"), "method");
        // line 138
        echo "    ";
    }

    public function getTemplateName()
    {
        return "partials/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  360 => 138,  357 => 137,  354 => 136,  351 => 135,  348 => 134,  345 => 133,  342 => 132,  339 => 131,  336 => 130,  333 => 129,  330 => 128,  327 => 127,  324 => 126,  319 => 122,  316 => 121,  312 => 119,  309 => 118,  306 => 117,  301 => 113,  296 => 114,  294 => 113,  290 => 112,  287 => 111,  284 => 110,  279 => 108,  274 => 107,  261 => 94,  255 => 92,  251 => 90,  249 => 89,  240 => 83,  235 => 81,  230 => 79,  225 => 77,  218 => 73,  214 => 72,  194 => 57,  174 => 39,  171 => 38,  167 => 28,  164 => 27,  161 => 26,  159 => 25,  156 => 24,  153 => 23,  150 => 22,  147 => 21,  144 => 20,  141 => 19,  138 => 18,  136 => 17,  133 => 16,  130 => 15,  120 => 29,  118 => 15,  113 => 13,  109 => 12,  104 => 9,  102 => 8,  94 => 7,  91 => 6,  88 => 5,  80 => 139,  78 => 126,  74 => 124,  72 => 121,  69 => 120,  67 => 117,  64 => 116,  62 => 110,  59 => 109,  56 => 108,  54 => 107,  49 => 104,  47 => 38,  41 => 34,  39 => 5,  34 => 3,  31 => 2,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set theme_config = attribute(config.themes, config.system.pages.theme) %}
<!DOCTYPE html>
<html lang=\"{{ grav.language.getLanguage ?: 'en' }}\">
<head>
{% block head %}
    <meta charset=\"utf-8\" />
    <title>{% if header.title %}{{ header.title|e('html') }} | {% endif %}{{ site.title|e('html') }}</title>
    {% include 'partials/metadata.html.twig' %}
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <link rel=\"icon\" type=\"image/png\" href=\"{{ url('theme://images/favicon.png') }}\" />
    <link rel=\"canonical\" href=\"{{ page.url(true, true) }}\" />

    {% block stylesheets %}
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        {% do assets.addCss('theme://css/bootstrap.min.css', 103) %}
        {% do assets.addCss('theme://css/font-awesome.min.css', 102) %}
        {% do assets.addCss('theme://css/animation.css', 101) %}
        {% do assets.addCss('theme://css/normalize.css', 100) %}
        {% do assets.addCss('theme://css/style.css', 100) %}
        {% do assets.addCss('theme://css/royalslider.css') %}
        {% do assets.addCss('theme://css/pace_theme.css') %}

        {% if browser.getBrowser == 'msie' and browser.getVersion >= 8 and browser.getVersion <= 9 %}
            {% do assets.addJs('theme://js/html5shiv-printshiv.min.js') %}
        {% endif %}
    {% endblock %}
    {{ assets.css() }}

    

{% endblock head %}
</head>

<body class=\"home\">
    <div id=\"wrapper\" class=\"win-min-height\">
        {% block header %}
        <header id=\"block01c\" class=\"header block background01\" data-module=\"Header\">
            <div class=\"container header_block\">
                <div class=\"row\">
                    <div class=\"social-icon\">
                        <ul>
                            <li>
                                <a class=\"colorWhite color01-hover03\" href=\"https://www.facebook.com/webinstantcom/\">
                                <i class=\"fa fa-facebook\">&nbsp;</i></a>
                            </li>
                            <li>
                                <a class=\"colorWhite color01-hover03\" href=\"https://twitter.com/WebInstantcom1\">
                                <i class=\"fa fa-twitter\">&nbsp;</i></a>
                            </li>
                        </ul>
                    </div>
                    <nav class=\"navbar navbar-default\">
                        <div class=\"container-fluid\">
                          <div class=\"navbar-header\">
                          <a class=\"navbar-brand\" href=\"{{ base_url }}\"><img class=\"img-responsive\" src=\"{{ url('theme://images/web-instant.png') }}\" alt=\"Web-instant\"></a>
                          <div class=\"menu-btn\">
                            <button type=\"button\" class=\"navbar-toggle collapsed colorWhite\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                              <strong>
                              <span class=\"sr-only\">Toggle navigation</span>
                              <span class=\"icon-bar background02\"></span>
                              <span class=\"icon-bar background02\"></span>
                              <span class=\"icon-bar background02\"></span>
                              </strong>
                             <strong> MENU</strong>
                            </button></div>
                          </div>
                          
                          <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                            <ul class=\"nav navbar-nav colorWhite\">
                              <li class=\"active\"><a class=\"colorWhite color01-hover03\" href=\"{{ base_url }}\">Home <span class=\"sr-only\">(current)</span></a></li>
                              <li><a class=\"colorWhite color01-hover03\" href=\"{{ base_url }}/blog\">Blog</a></li>
                              <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle colorWhite color01-hover03\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">Services <span class=\"caret\"></span></a>
                                <ul class=\"dropdown-menu background01\" role=\"menu\">
                                  <li><a class=\"colorWhite color01-hover03\" href=\"{{ base_url }}/custom-web-application\">Custom web aplication</a></li>
                                  <li class=\"divider\"></li>
                                  <li><a class=\"colorWhite color01-hover03\" href=\"{{ base_url }}/wordpress\">Wordpress</a></li>
                                  <li class=\"divider\"></li>
                                  <li><a class=\"colorWhite color01-hover03\" href=\"{{ base_url }}/coming-soon-landing-page\">Coming soon page/ Landing page</a></li>
                                  <li class=\"divider\"></li>
                                  <li><a class=\"colorWhite color01-hover03\" href=\"{{ base_url }}/invitation-online\">Invitation Online</a></li>
                                  <li class=\"divider\"></li>
                                  <li><a class=\"colorWhite color01-hover03\" href=\"#\"></a></li>
                                </ul>
                              </li>
                              <li>
                                {% if base_url == page.url %}
                                    <a class=\"colorWhite color01-hover03 goto\" href=\"#\" data-goto=\"block12e\">Contact Us</a>
                                {% else %}
                                    <a class=\"colorWhite color01-hover03 goto\" href=\"{{ base_url }}#block12e\">Contact Us</a>
                                {% endif %}
                              </li>
                            </ul>
                            
                          </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        {% endblock %}
        


        {% block showcase %}{% endblock %}
        {% block feature %}{% endblock %}

        {% block body %}

        <section id=\"body\" class=\"{{ class }}\">
            {% block content %}{% endblock %}
        </section>
        {% endblock %}

        {% block footer %}
            {% include 'partials/footer.html.twig' %}
        {% endblock %}

        {% block bottom %}

        {% endblock %}

    </div>
    {% block javascripts %}
        {% do assets.addJs('theme://js/jquery-1.11.2.min.js', 100) %}
        {% do assets.addJs('theme://js/bootstrap.min.js', 100) %}
        {% do assets.addJs('theme://js/pace.min.js', 100) %}
        {% do assets.addJs('theme://js/responsive-slider.js') %}
        {% do assets.addJs('theme://js/countUp.js') %}
        {% do assets.addJs('theme://js/touch-slide.js') %}
        {% do assets.addJs('theme://js/modernizr.js') %}
        {% do assets.addJs('theme://js/classie.js') %}
        {% do assets.addJs('theme://js/jquery.custom-scrollbar.js') %}
        {% do assets.addJs('theme://js/royalslider-min.js') %}
        {% do assets.addJs('theme://js/custom.js') %}
    {% endblock %}
    {{ assets.js() }}
</body>
</html>", "partials/base.html.twig", "C:\\xampp7\\htdocs\\web-instant.com\\user\\themes\\webinstant\\templates\\partials\\base.html.twig");
    }
}
