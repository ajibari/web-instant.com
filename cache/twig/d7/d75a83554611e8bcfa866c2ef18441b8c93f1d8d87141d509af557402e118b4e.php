<?php

/* @Page:C:/xampp7/htdocs/web-instant.com/user/pages/06.about-us */
class __TwigTemplate_a2b0302656c724b3cc6037db8968d5660ee5d40a7ed6aa9088a7f0457bb791ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
      \t<div class=\"row\">
        <div class=\"feature animated fadeIn slow\">
        \t<h2>we love what we do</h2>
            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
            <a href=\"javascript:void(0)\" class=\"btn btn-default more color01 background08 color01-hover border-color07\">LEARN MORE</a>
        </div>
        \t<div class=\"boxes animated fadeIn slow\">
              <div class=\"img-area col-md-10 col-md-offset-1\">
              <img class=\"img-responsive\" src=\"";
        // line 10
        echo $this->getAttribute(($context["post"] ?? null), "url", array());
        echo "images/work09.jpg\" alt=\"Work\">
              </div> 
            </div>
</div>";
    }

    public function getTemplateName()
    {
        return "@Page:C:/xampp7/htdocs/web-instant.com/user/pages/06.about-us";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 10,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container\">
      \t<div class=\"row\">
        <div class=\"feature animated fadeIn slow\">
        \t<h2>we love what we do</h2>
            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
            <a href=\"javascript:void(0)\" class=\"btn btn-default more color01 background08 color01-hover border-color07\">LEARN MORE</a>
        </div>
        \t<div class=\"boxes animated fadeIn slow\">
              <div class=\"img-area col-md-10 col-md-offset-1\">
              <img class=\"img-responsive\" src=\"{{post.url}}images/work09.jpg\" alt=\"Work\">
              </div> 
            </div>
</div>", "@Page:C:/xampp7/htdocs/web-instant.com/user/pages/06.about-us", "");
    }
}
