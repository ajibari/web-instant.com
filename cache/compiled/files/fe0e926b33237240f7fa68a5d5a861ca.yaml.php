<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp7/htdocs/web-instant.com/user/themes/webinstant/blueprints.yaml',
    'modified' => 1501747298,
    'data' => [
        'name' => 'WebInstant',
        'version' => '1.0.0',
        'description' => 'WebInstant is a custom theme',
        'icon' => 'empire',
        'author' => [
            'name' => 'Bari',
            'email' => 'ajibari@gmail.com',
            'url' => 'http://www.ajibari.com'
        ],
        'homepage' => 'https://github.com/ajibari',
        'demo' => NULL,
        'keywords' => 'web-instant, theme, core, modern, fast, responsive, html5, css3',
        'bugs' => 'https://github.com/ajibari/grav-theme-webinstant/issues',
        'license' => 'MIT',
        'form' => [
            'validation' => 'loose',
            'fields' => [
                'dropdown.enabled' => [
                    'type' => 'toggle',
                    'label' => 'Dropdown in navbar',
                    'highlight' => 1,
                    'default' => 1,
                    'options' => [
                        1 => 'PLUGIN_ADMIN.ENABLED',
                        0 => 'PLUGIN_ADMIN.DISABLED'
                    ],
                    'validate' => [
                        'type' => 'bool'
                    ]
                ]
            ]
        ]
    ]
];
