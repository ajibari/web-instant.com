<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp7/htdocs/web-instant.com/user/accounts/barrey.yaml',
    'modified' => 1501730127,
    'data' => [
        'email' => 'ajibari@gmail.com',
        'fullname' => 'Ridwan Aji Bari',
        'title' => 'Administrator',
        'state' => 'enabled',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'hashed_password' => '$2y$10$Lv/XVjHLdjs/ZR.lTMCpheT8otFEIW8PhWC2D6M.YFCGsh0G3kpy2'
    ]
];
