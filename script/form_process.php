<?php

if(!empty($_POST['is_submit_quote'])){
	
	require './PHPMailer/PHPMailerAutoload.php';

	//validation reCaptcha
	if(empty($_POST['g-recaptcha-response'])){
		$response = array('status' => 404);
		echo json_encode($response);
		exit;
	}

	//sending email to cs@web-instant.com and client's email
	$email = array('receiver' => 'cs@web-instant.com', 'bcc' => 'ajibari@gmail.com');
	$config = smtpConfig();
	$message = "Salah satu calon klien anda mengirimkan guestbook. <br/>".$_POST['name'].'<br/>'.$_POST['email'].'<br/>'.$_POST['problem'].'<br/>'.$_POST['budget'].'<br/>'.$_POST['message'];
	$data = array('subject' => 'Guestbook Web-instant', 'message' => $message);

	sendingMail($email, $config, $data);

	//sending autoreply to client
	$email_client = array('receiver' => $_POST['email']);
	$reply_data = array('subject' => 'Thanks for contacting us', 'msg_html' => 'autoreply.html');
	sendingMail($email_client, $config, $reply_data);

	//sending response to client
	$response = array('message' => 'success');
	echo json_encode($response);	
	exit;
}

$response = array('status' => 404);
echo json_encode($response);
exit;


function sendingMail($email, $config, $data){

	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail->CharSet = $config['charset'];
	$mail->SMTPDebug = $config['smtp_debug'];
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = $config['debug_output'];
	//Set the hostname of the mail server
	$mail->Host = $config['host'];
	//Set the SMTP port number - likely to be 25, 465 or 587
	$mail->Port = $config['port'];
	//Whether to use SMTP authentication
	$mail->SMTPAuth = $config['smtp_auth'];
	//Username to use for SMTP authentication
	$mail->Username = $config['username'];
	//Password to use for SMTP authentication
	$mail->Password = $config['password'];
	//Set who the message is to be sent from
	$mail->setFrom('no-reply@web-instant.com', 'Web-instant.com Bot');
	//Set an alternative reply-to address
	$mail->addReplyTo('cs@web-instant.com', 'Customer Service');
	//Set who the message is to be sent to
	$mail->addAddress($email['receiver'], 'John Doe');
	//Set the subject line
	$mail->Subject = $data['subject'];
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	if(!empty($data['msg_html'])){
		$mail->msgHTML(file_get_contents($data['msg_html']), dirname(__FILE__));
	}
	//Replace the plain text body with one created manually
	if(!empty($data['message'])){
		$mail->AltBody = $data['message'];
		$mail->Body = $data['message'];
	}
	//Attach an image file
	if(!empty($data['attachment'])){
		$mail->addAttachment($data['attachment']);
	}

	//send to BCC
	if(!empty($email['bcc'])){
		$mail->addCustomHeader("BCC: ".$email['bcc']); 
	}

	//send the message, check for errors
	if (!$mail->send()) {
	    echo "Mailer Error: " . $mail->ErrorInfo;
	} else {
	    return true;
	}

}

function smtpConfig(){
	$config = array('host' => 'ssl://srv25.niagahoster.com', 
		'charset' => 'UTF-8',
		'smtp_debug' => 0,
		'debug_output' => 'html',
		'port' => 465,
		'smtp_auth' => true,
		'username' => 'no-reply@web-instant.com',
		'password' => 'tekanenter',
		'email_from' => 'no-reply@web-instant.com',
		'email_reply' => 'cs@web-instant.com',
		);

	return $config;
}
